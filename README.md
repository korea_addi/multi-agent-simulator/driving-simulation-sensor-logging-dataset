# KETI DRIVING SIMULATION SENSOR LOGGING DATASET

## 소개

KETI DRIVING SIMULATION SENSOR LOGGING DATASET은 자율주행 기술에 필수적으로 사용되는 인공지능 기반의 인식 알고리즘을 테스트하기 위해 시뮬레이터상의 가상의 도로 주행환경에 자율주행에 필요한 센서와 센서가 부착된 차량을 생성하여 인식 GT(Ground Truth) 값과 함께 제공하는 것을 목표로 합니다. 이를 위해 한국의 도로 환경 중 K-city를 가상의 컨텐츠로 구현하고 해당 도로환경에서의 다양한 주행 상황을 포함하고 있습니다.

KETI DRIVING SIMULATION SENSOR LOGGING DATASET을 통해 제공하고자하는 목표는 다음과 같습니다.

- 국내 도로 환경의 가상 세계(컨텐츠) 구축을 통한 주행 상황 제공
- 인공지능 기반 인지, 판단 알고리즘의 실행중 검증에 필요한 데이터 제공
- 가상 카메라 센서 기반 알고리즘의 실행중 검증 데이터 제공
- 가상 라이다 센서 기반 알고리즘의 실행중 검증 데이터 제공

### Feature

- KETI DRIVING SIMULATION SENSOR LOGGING DATASET은 대한민국 주행환경을 포함하는 데이터입니다.
- 연속된 시간에 따른 데이터를 제공합니다.
- 자율주행에 필요한 센서 데이터를 제공합니다.
- 다양한 태스크를 검증하기 위한 데이터를 제공합니다. (2D/3D Detection, Segmentation, Prediction, Etc)
- ROS(Robot Operating System)에서 사용되는 포맷으로 데이터를 제공합니다.

1. 아래와 같은 센서 데이터가 제공됩니다.
    - RGB 카메라 센서에서 취득한 이미지 데이터
    - LiDAR 센서에서 취득한 point cloud 데이터
    - GPS 센서에서 취득한 위치 측정 데이터
    - IMU 센서에서 취득한 자세 데이터
2. 아래와 같은 검증을 위한 데이터가 제공됩니다.
    - 교통 객체 위치, 종류, 3D bounding box 등
3. 데이터셋 취득은 다음과 같이 다양한 환경에서 취득되었습니다.  
    ① 데이터 취득 대상 장소
      : K-City 자율주행 테스트베드
    ② 데이터 취득 대상 환경
      : 주간  

## DATASET

DATASET은 아래와 같습니다.

- json포맷의 센서 구성 현황 데이터: vehicle.json
    - 파일 링크: http://database.ketiauto.kr:5000/sharing/lcnpQXIP9
- 5분 길이로 K-City에서의 주행상황이 기록된 ROS bag 파일
    - 파일 1  링크: http://database.ketiauto.kr:5000/sharing/8vvhQDgKO
    - 파일 2  링크: http://database.ketiauto.kr:5000/sharing/aAsXGMPnK
    - 파일 3  링크: http://database.ketiauto.kr:5000/sharing/yT638SiI5
    - 파일 4  링크: http://database.ketiauto.kr:5000/sharing/fyH40siVk
    - 파일 5  링크: http://database.ketiauto.kr:5000/sharing/g7B9odWWI
    - 파일 6  링크: http://database.ketiauto.kr:5000/sharing/2ViRdkFPa
    - 파일 7  링크: http://database.ketiauto.kr:5000/sharing/WhFFLWXVx
    - 파일 8  링크: http://database.ketiauto.kr:5000/sharing/kx1LgOgVR
    - 파일 9  링크: http://database.ketiauto.kr:5000/sharing/5SsuhZv9F
                                        